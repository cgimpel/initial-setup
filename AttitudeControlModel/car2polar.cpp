#include "configure.h"

double car2polar_r(double x, double y, double z) {
	double r = sqrt((pow(x, 2) + pow(y, 2) + pow(z, 2)));
	return r;
}

double car2polar_theta(double x, double y, double z) {
	double r = car2polar_r(x, y, z);
	double theta = acos(z / r);
	return theta;
}

double car2polar_phi(double x, double y, double z) {
	double phi = atan(y / x);
	return phi;
}

vec car2polar(PositionClass posObj) {
	double x = posObj.ECEF.x;
	double y = posObj.ECEF.y;
	double z = posObj.ECEF.z;

	double phi = car2polar_phi(x, y, z);
	double theta = car2polar_theta(x, y, z);
	double r = car2polar_r(x, y, z);

	vec PhiThetaR(3);
	PhiThetaR = { phi, theta, r };
	return PhiThetaR;
}