#pragma once
#include "configure.h"
class FrameworkClass
{
public:
	// not done
	OrbitalParametersClass satelliteOrbitalParameters;
	SystemTimeClass systemTime;
	PositionClass satellitePosition;
	EarthMagneticFieldClass earthMagneticField;
	SatelliteAttitudeClass satelliteAttitude;
	SatelliteStructuralClass satelliteStructural;
	OrbitalParametersClass sunOrbitalParameters;
	PositionClass sunPosition;
	PowerHarvestingClass powerHarvesting;
	RadioTransmissionClass radioTransmission;

};

