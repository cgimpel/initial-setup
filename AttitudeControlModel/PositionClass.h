#pragma once
class PositionClass
{
public:
	//Needs to be verified. Also look more into dcmeci2ecef
	double DistanceFromBodyToOrigin;	//[m]
	xyzVectorClass ECI;					//[m] in IAU2000/2006
	xyzVectorClass ECEF;				//[m]

	void SetECI(PositionClass obj, vec val) {
		obj.ECI.x = val(0);
		obj.ECI.y = val(1);
		obj.ECI.z = val(2);
	}

	void SetECEF(PositionClass obj, vec val) {
		obj.ECEF.x = val(0);
		obj.ECEF.y = val(1);
		obj.ECEF.z = val(2);
	}

	void CalculatePosition(PositionClass PosObj, OrbitalParametersClass OrbObj, SystemTimeClass TimeObj) {

		mat ECI2ECEF_Matrix(3, 3, fill::zeros);
		mat rotationMatrix(3, 3, fill::zeros);

		PosObj.DistanceFromBodyToOrigin = OrbObj.Semimajor_Axis*(1 - pow(OrbObj.Eccentricity, 2.0)) / (1 + OrbObj.Eccentricity*cos(OrbObj.trueAnomaly));
		vec cartCoordOnOrbitalPlane = { PosObj.DistanceFromBodyToOrigin*cos(OrbObj.trueAnomaly), PosObj.DistanceFromBodyToOrigin*sin(OrbObj.trueAnomaly), 0 };

		rotationMatrix = OrbitalPlaneToECIDCM(OrbObj.Inclination, OrbObj.RAAN, OrbObj.Arg_of_Perigee);
		PosObj.ECI.vector = cartCoordOnOrbitalPlane * rotationMatrix;

		PosObj.SetECI(PosObj, PosObj.ECI.vector); //Not confirmed
		//ECI2ECEF_Matrix = dcmeci2ecef(); Definitely needs more research
		PosObj.ECEF = ECI2ECEF_Matrix * PosObj.ECI.vector;
		
	}
};

