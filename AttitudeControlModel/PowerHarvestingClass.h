#pragma once
#include "configure.h"
class PowerHarvestingClass
{
public:
	//needs to be confirmed
	bool inEclipse;					//[boolean]
	double SolarPanelEfficiency;	//[m]

	void ShadowDetection(PowerHarvestingClass PowerObj, PositionClass SunPos, PositionClass SatPos) {

		vec sunToSatVector = SunPos.ECI.vector + SatPos.ECI.vector;
		double sunToEarthEdgeAngle = atan(EARTH_RADIUS / SunPos.DistanceFromBodyToOrigin);
		double sunToSatAngle = acos(norm(sunToSatVector) / SunPos.DistanceFromBodyToOrigin);

		if (sunToSatAngle < sunToEarthEdgeAngle) {
			if (norm(sunToSatVector) > SunPos.DistanceFromBodyToOrigin) {
				PowerObj.inEclipse = 1;
			}
			else {
				PowerObj.inEclipse = 0;
			}
		}
		else {
			PowerObj.inEclipse = 0;
		}
	}
};

