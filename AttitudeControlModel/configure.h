// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file

#ifndef PCH_H
#define PCH_H

// Headers used
#include <armadillo>
#include <ctime>
#include <iostream>
#include <string>
#include <cmath>
#include <math.h>
#include "constants.h"

//Boost
//#include <boost>
#include "boost/date_time/gregorian/gregorian.hpp"
#include "boost/date_time/posix_time/posix_time.hpp"
#include <boost/geometry.hpp>
#include "boost/math/special_functions/bessel.hpp"

using namespace arma;
using namespace boost;

// Classes used
#include "SystemTimeClass.h"


#include "OrbitalParametersClass.h"
#include "PositionClass.h"
#include "EarthMagneticFieldClass.h"
#include "SatelliteAttitudeClass.h"
#include "SatelliteStructuralClass.h"
#include "PowerHarvestingClass.h"
#include "RadioTransmissionClass.h"
#include "xyzVectorClass.h"
#include "EulerVectorClass.h"

// Functions used
vec car2polar(PositionClass posObj);
double rad2deg(double x);
mat OrbitalPlaneToECIDCM(double inclination, double RAAN, double argOfPerigee);

//#include "FrameworkClass.h"





#endif //PCH_H