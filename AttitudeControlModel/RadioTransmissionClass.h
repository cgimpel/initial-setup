#pragma once
#include "configure.h"
// This class is defined to capture all variables related to keplers orbital model.
// It also contains methods to calculate the orbital parameters like true anomaly and mean anomaly that 
// are used for position determination.
class RadioTransmissionClass
{
public:
	// Needss to be verified
	xyzVectorClass positionOfCHIME_ECEF;
	xyzVectorClass positionOfGroundStation_ECEF;
	xyzVectorClass satelliteToCHIME_ECEF;
	vec data = vec(3,fill::zeros);

	void SetPositionOfCHIME_ECEF(RadioTransmissionClass obj, vec value) {
		obj.positionOfCHIME_ECEF.x = value(0);
		obj.positionOfCHIME_ECEF.y = value(1);
		obj.positionOfCHIME_ECEF.z = value(2);
	}
	void SetPositionOfGroundStation_ECEF(RadioTransmissionClass obj, vec value) {
		obj.positionOfGroundStation_ECEF.x = value(0);
		obj.positionOfGroundStation_ECEF.y = value(1);
		obj.positionOfGroundStation_ECEF.z = value(2);
	}
	void SetPositionOfCHIME_ECEF(RadioTransmissionClass obj, vec value) {
		obj.positionOfCHIME_ECEF.x = value(0);
		obj.positionOfCHIME_ECEF.y = value(1);
		obj.positionOfCHIME_ECEF.z = value(2);
	}

	void RadioTransmissionClass() {
		data << -164886.009300717 << 3731235.05547077 << -5162347.51927725 << endr;
	}

	void SatelliteToChimeVector(RadioTransmissionClass RadTransObj, PositionClass SatPos) {
		RadTransObj.satelliteToCHIME_ECEF = SatPos.ECEF - RadTransObj.positionOfCHIME_ECEF;
	}
};

