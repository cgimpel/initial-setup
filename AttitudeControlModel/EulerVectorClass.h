#pragma once
#include "configure.h"
class EulerVectorClass
{
	//More or less done but needs verification
public:
	double pitch;
	double yaw;
	double roll;

	mat vector;

	//not sure I need this declaration
	/*EulerVectorClass(double Pitch, double Roll, double Yaw)
	{
		pitch = Pitch;
		roll = Roll;
		yaw = Yaw;
	}
	EulerVectorClass()
	{
		pitch = 0;
		roll = 0;
		yaw = 0;
	}*/

	mat PopulateVector(EulerVectorClass obj) {
		vector = mat(1, 3, fill::zeros);

		vector(0, 0) = obj.pitch;
		vector(0, 1) = obj.yaw;
		vector(0, 2) = obj.roll;
		return vector;
	}

	//Needs verification
	// Matrix multiplication of this class type
	mat mtimes(EulerVectorClass obj2, EulerVectorClass obj1) {

		mat result = obj2.vector * obj1.vector.t;
		return result;
	}

	// Needs verification
	// Addition between two xyzVectorClasses
	mat plus(EulerVectorClass obj1, EulerVectorClass obj2) {
		mat result = obj1.vector + obj2.vector;
		return result;
	}

	// Needs verification
	// Subtraction between two xyzVectorClasses
	mat minus(EulerVectorClass obj1, EulerVectorClass obj2) {
		mat result = obj1.vector - obj2.vector;
		return result;
	}
};

