#pragma once
#include "configure.h"
class SatelliteStructuralClass
{
public:
	//Not done
	// needs to be updated away from hysteresis rods and filled in with magnetorquers
	xyzVectorClass satelliteMagneticField_Body;		// Body reference frame
	xyzVectorClass satelliteHysteresisRod1_Body;	
	xyzVectorClass satelliteHysteresisRod2_Body;
	mat inertiaMatrix_Body = mat(3, 3, fill::zeros);

	void SetSatelliteMagneticField_Body(SatelliteStructuralClass obj, vec value) {
		obj.satelliteMagneticField_Body.x = value(0);
		obj.satelliteMagneticField_Body.y = value(1);
		obj.satelliteMagneticField_Body.z = value(2);
	}
	void SetSatelliteHysteresisRod1_Body(SatelliteStructuralClass obj, vec value) {
		obj.satelliteHysteresisRod1_Body.x = value(0);
		obj.satelliteHysteresisRod1_Body.y = value(1);
		obj.satelliteHysteresisRod1_Body.z = value(2);
	}
	void SetSatelliteHysteresisRod2_Body(SatelliteStructuralClass obj, vec value) {
		obj.satelliteHysteresisRod2_Body.x = value(0);
		obj.satelliteHysteresisRod2_Body.y = value(1);
		obj.satelliteHysteresisRod2_Body.z = value(2);
	}

	SatelliteStructuralClass(vec MagneticMoment, vec HysteresisRod1, vec HysteresisRod2, mat InertiaMatrix) {
		satelliteMagneticField_Body.vector = MagneticMoment;
		satelliteHysteresisRod1_Body.vector = HysteresisRod1;
		satelliteHysteresisRod2_Body.vector = HysteresisRod2;
		inertiaMatrix_Body = InertiaMatrix;
	}
	SatelliteStructuralClass() {
		satelliteMagneticField_Body.vector = vec(3,fill::zeros);
		satelliteHysteresisRod1_Body.vector = vec(3, fill::zeros);
		satelliteHysteresisRod2_Body.vector = vec(3, fill::zeros);
		inertiaMatrix_Body = mat(3,3, fill::zeros);
	}

};

