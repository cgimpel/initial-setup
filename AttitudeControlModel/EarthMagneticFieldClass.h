#pragma once
#include "configure.h"
class EarthMagneticFieldClass
{
public:
	//Completed but needs verification
	xyzVectorClass EarthMagneticField_ECEF; // [Teslas]
	
	EarthMagneticFieldClass EarthsMagneticFieldAtPosition(EarthMagneticFieldClass magneticFieldObj, PositionClass posObj, SystemTimeClass timeObj) {
		vec PhiThetaR = car2polar(posObj);
		
		posix_time::ptime Jan_1st = posix_time::ptime(gregorian::date(2015, gregorian::Jan, 1), posix_time::time_duration(0, 0, 0)); //[1-Jan-2015 00:00:00]
		gregorian::days Days_since_Jan_1st_2015 = timeObj.DateAndTime.date - Jan_1st.date; //Unconfirmed
		
		vec MagneticFieldStrength = IGRF_Model(PhiThetaR(2), rad2deg(PhiThetaR(0)), rad2deg(PhiThetaR(1)));
		xyzVectorClass magneticFieldObj.EarthMagneticField_ECEF = polar2car(MagneticFieldStrength);

		return magneticFieldObj;
	}

	double TimeStepCalc(SystemTimeClass oldTime, SystemTimeClass newTime) {
		double TimeStep = newTime.TimeSinceLaunch - oldTime.TimeSinceLaunch;
		return TimeStep;
	}

	void SetEarthMagneticField(EarthMagneticFieldClass obj, vec val) {
		obj.EarthMagneticField_ECEF.x = val(0);
		obj.EarthMagneticField_ECEF.y = val(1);
		obj.EarthMagneticField_ECEF.z = val(2);
	}
};

