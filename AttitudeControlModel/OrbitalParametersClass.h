#pragma once
#include "configure.h"
class OrbitalParametersClass
{
public:
	//Needs to be confirmed

	//Satellite Orbital Properties
	double Eccentricity;	//[Unitless]
	double Inclination;		//[rad]
	double Semimajor_Axis;	//[m]
	double RAAN;			//[rad]
	double Arg_of_Perigee;	//[rad]
	double Gravitational_Parameter; //[m^3 s^-2]

public:
	double trueAnomaly;		//[rad]
	double meanAnomaly;		//[rad]
	time_t epoch;			//[datetime]

	OrbitalParametersClass(double eccentricity, double inclination, double semimajorAxis, double raan, double argPerigee, double gm)
	{
		Eccentricity = eccentricity;
		Inclination = inclination;
		Semimajor_Axis = semimajorAxis;
		RAAN = raan;
		Arg_of_Perigee = argPerigee;
		Gravitational_Parameter = gm;
	}
	OrbitalParametersClass()
	{
		Eccentricity = 0;
		Inclination = 0;
		Semimajor_Axis = 0;
		RAAN = 0;
		Arg_of_Perigee = 0;
		Gravitational_Parameter = 0;
	}

	// Determine mean anomaly based off of objects epoch and current date and time
	double MeanAnomalyFromTime(OrbitalParametersClass OrbObj, SystemTimeClass TimeObj) {
		time_t timeSinceEpoch = TimeObj.DateAndTime - OrbObj.epoch;
		double orbitalPeriod = sqrt((4 * (pow(M_PI,2))*pow(OrbObj.Semimajor_Axis,3)) / (OrbObj.Gravitational_Parameter));
		meanAnomaly = (2 * M_PI*(timeSinceEpoch / orbitalPeriod)) % (2 * M_PI);
		return meanAnomaly;
	}

	// Determine epoch from mean anomaly and orbital parameters
	double EpochFromMeanAnomaly(OrbitalParametersClass OrbObj, SystemTimeClass TimeObj) {
		time_t timeSinceEpoch = (OrbObj.meanAnomaly / (2 * M_PI))*sqrt((4 * pow(M_PI,2))*pow(OrbObj.Semimajor_Axis,3)) / (OrbObj.Gravitational_Parameter)); //(M/2pi)*Orbital Period
		epoch = TimeObj.DateAndTime - timeSinceEpoch;
		return epoch;
	}

	// Calculate mean anomaly from true anomaly and orbital parameters
	double MeanAnomalyFromTrueAnomaly(OrbitalParametersClass OrbObj) {
		meanAnomaly = OrbObj.trueAnomaly - 2 * OrbObj.Eccentricity*sin(OrbObj.trueAnomaly);
		return meanAnomaly;
	}

	// Calculates the true anomaly from the mean anomaly and orbital parameters
	double TrueAnomalyFromMeanAnomaly(OrbitalParametersClass OrbObj) {
		double eccentricAnomaly = OrbObj.meanAnomaly;
		for (int i = 0; i < NUM_TERMS_IN_INF_SERIES; i++)
		{
			eccentricAnomaly = eccentricAnomaly + (2 / i) * boost::math::cyl_bessel_j(i, i*OrbObj.Eccentricity)*sin(i*OrbObj.meanAnomaly); //Not confirmed
		}
		double trueAnomaly = 2 * atan(sqrt((1 + OrbObj.Eccentricity) / (1 - OrbObj.Eccentricity))*tan(eccentricAnomaly / 2)); //not confirmed
	}

	//Propagates orbit forward using the orbital parameters and the current time as variables to calculate the new position
	OrbitalParametersClass OrbitPropagator(OrbitalParametersClass OldOrbitalObj, SystemTimeClass NewSystemTimeObj) {
		OrbitalParametersClass NewOrbitalParametersObj = OldOrbitalObj;
		NewOrbitalParametersObj.meanAnomaly = MeanAnomalyFromTime(NewOrbitalParametersObj, NewSystemTimeObj);
		NewOrbitalParametersObj.trueAnomaly = TrueAnomalyFromMeanAnomaly(NewOrbitalParametersObj);
		return NewOrbitalParametersObj;
	}
};

