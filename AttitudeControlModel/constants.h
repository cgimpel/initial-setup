#pragma once

const int NUM_TERMS_IN_INF_SERIES = 10;
const double M_PI = arma::datum::pi;

const double EARTH_RADIUS = 6371200.0;