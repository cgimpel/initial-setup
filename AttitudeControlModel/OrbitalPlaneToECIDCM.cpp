#include "configure.h"

mat OrbitalPlaneToECIDCM(double inclination, double RAAN, double argOfPerigee) {
	mat DCM = R3DCM(argOfPerigee)*R1DCM(inclination)*R3DCM(RAAN);
	return DCM;
}

mat R1DCM(double angle) {
	mat R1(3, 3, fill::zeros);
	
	R1 << 1 << 0 << 0 << endr
		<< 0 << cos(angle) << sin(angle) << endr
		<< 0 << -sin(angle) << cos(angle) << endr;
	return R1;
}

mat R2DCM(double angle) {
	mat R2(3, 3, fill::zeros);

	R2 << cos(angle) << 0 << -sin(angle) << endr
		<< 0 << 1 << 0 << endr
		<< sin(angle) << 0 << cos(angle) << endr;
	return R2;
}

mat R3DCM(double angle) {
	mat R3(3, 3, fill::zeros);

	R3 << cos(angle) << sin(angle) << 0 << endr
		<< -sin(angle) << cos(angle) << 0 << endr
		<< 0 << 0 << 1 << endr;
	return R3;
}