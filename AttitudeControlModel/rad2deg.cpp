#include "configure.h"

double rad2deg(double x) {
	double deg = x * 180.0 / (2.0*M_PI);
	return deg;

}